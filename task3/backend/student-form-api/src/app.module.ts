import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import * as Joi from '@hapi/joi';
import { ApplicationModule } from './application/application.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Application } from './database/entities/application.entity';

@Module({
  imports: 
  [
      TypeOrmModule.forRoot({
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: 'frequency!337',
        database: 'studentApplication',
        entities: [__dirname + '/**/*.entity{.ts,.js}'],
        synchronize: false,
      }),
      TypeOrmModule.forFeature([Application]),
  ApplicationModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {

}

