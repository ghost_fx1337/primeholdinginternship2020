import { CommunicationPreference } from './../../enums/communication-preference.enum';
import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { EnglishLevel } from 'src/enums/english-level.enum';

@Entity('applications')
export class Application {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @Column('nvarchar')
    firstName: string;

    @Column('nvarchar')
    surname: string;

    @Column()
    age: number;

    @Column()
    phoneNumber: string;

    @Column({ type: 'enum', enum: CommunicationPreference })
    communicationPreference: CommunicationPreference;

    @Column({ type: 'enum', enum: EnglishLevel })
    englishLevel: EnglishLevel

    @Column()
    startDate: string

    @Column()
    skillsAndCourses: string

    @Column()
    presentation: string
    
    @Column({ type: 'boolean'})
    studyFromHome: boolean

    @Column({ type: 'boolean', default: false })
    isDeleted: boolean;
}