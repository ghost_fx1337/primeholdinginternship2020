import { Injectable, NotFoundException } from '@nestjs/common';
import { Application } from 'src/database/entities/application.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ApplicationDTO } from './dtos/application.dto';
import * as moment from 'moment';

@Injectable()
export class ApplicationService {

    constructor(
        @InjectRepository(Application)
        private readonly applicationRepo: Repository<Application>,
    ) {}

    async getAllApplications(): Promise<ApplicationDTO[]> {
        const applications = await this.applicationRepo.find({
            where: {isDeleted: false}
        });
        if (!applications) {
            throw new NotFoundException('There are no applications currently in the system!')
        }
        return applications;
    }

    async getApplication(id: string): Promise<ApplicationDTO> {
        const application = await this.applicationRepo.findOne({
            where: { id, isDeleted: false},
        });
        if (!application) {
            throw new NotFoundException('Such an application does not exist in the system!')
        }
        return application;
    }

    async createApplication(applicationDTO: ApplicationDTO): Promise<ApplicationDTO> {
        console.log(applicationDTO);
        const application = new Application();
        application.firstName = applicationDTO.firstName;
        application.surname = applicationDTO.surname;
        application.age = applicationDTO.age;
        application.phoneNumber = applicationDTO.phoneNumber;
        application.communicationPreference = applicationDTO.communicationPreference;
        application.englishLevel = applicationDTO.englishLevel;
        application.startDate = moment(applicationDTO.startDate).format('YYYY-MM-DD');
        application.skillsAndCourses = applicationDTO.skillsAndCourses;
        application.presentation = applicationDTO.presentation;
        application.studyFromHome = applicationDTO.studyFromHome;

        const savedApplication = await this.applicationRepo.save(application);
    
        return savedApplication;
    }

    async updateProject(id: string, applicationDTO: ApplicationDTO): Promise<ApplicationDTO> {
        if (+id !== +applicationDTO.id) {
        throw new NotFoundException('No such application in the system!');
        }
        let application = await this.applicationRepo.findOne({
        where: { id, isDeleted: false},
        });
    
        application.firstName = applicationDTO.firstName;
        application.surname = applicationDTO.surname;
        application.age = applicationDTO.age;
        application.phoneNumber = applicationDTO.phoneNumber;
        application.communicationPreference = applicationDTO.communicationPreference;
        application.englishLevel = applicationDTO.englishLevel;
        application.startDate = moment(applicationDTO.startDate).format('YYYY-MM-DD');
        application.skillsAndCourses = applicationDTO.skillsAndCourses;
        application.presentation = applicationDTO.presentation;
        application.studyFromHome = applicationDTO.studyFromHome;

        const savedProject = await this.applicationRepo.save(application);
        
        return savedProject;
    }

    async deleteApplication(id: string): Promise<ApplicationDTO> {
        const found = await this.applicationRepo.findOne({
            where: { id, isDeleted: false },
        });
    
        found.isDeleted = true;
        await this.applicationRepo.save(found);
        return found;
    }
}
