import { EnglishLevel } from './../../enums/english-level.enum';
import { CommunicationPreference } from './../../enums/communication-preference.enum';
import { Expose } from 'class-transformer';
export class ApplicationDTO {

@Expose()
id: string

@Expose()
firstName: string

@Expose()
surname: string

@Expose()
age: number

@Expose()
phoneNumber: string

@Expose()
communicationPreference: CommunicationPreference

@Expose()
englishLevel: EnglishLevel

@Expose()
startDate: string

@Expose()
skillsAndCourses: string

@Expose()
presentation: string

@Expose()
studyFromHome: boolean

}