import {
    Controller,
    Post,
    Body,
    Get,
    Param,
    Patch,
    Delete,
  } from '@nestjs/common';
import { ApplicationService } from './application.service';
import { APIResponse } from './dtos/api-response.dto';
import { ApplicationDTO } from './dtos/application.dto';

@Controller('applications')
export class ApplicationController {
    constructor(private readonly appService: ApplicationService) {}

    @Get()
    async getAllApplications(): Promise<APIResponse<ApplicationDTO[]>> {
    return this.serializeResponse(await this.appService.getAllApplications());
    }

    @Get(':id')
    async getApplication(@Param('id') id: string): Promise<APIResponse<ApplicationDTO>> {
    return this.serializeResponse(await this.appService.getApplication(id));
    }

    @Post()
    async createApplication(
        @Body() applicationDTO: ApplicationDTO): Promise<APIResponse<ApplicationDTO>> {
    return this.serializeResponse(await this.appService.createApplication(applicationDTO));
    }

    @Delete(':id')
    async deleteApplication(@Param('id') id: string): Promise<APIResponse<ApplicationDTO>>{
    return this.serializeResponse(await this.appService.deleteApplication(id));
    }

    @Patch(':id')
    async updateApplication(
    @Param('id') id: string,
    @Body() applicationDTO: ApplicationDTO,
    ): Promise<APIResponse<ApplicationDTO>> {
        console.log(applicationDTO)
    return this.serializeResponse(
        await this.appService.updateProject(id, applicationDTO),
    );
    }

    private serializeResponse<T>(data: T): APIResponse<T> {
    return { data };
    }
}
