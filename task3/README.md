# Student Application Manager

## Description:

Application management system with full CRUD capabilities:

`features:`
- Create
- Read
- Update
- Delete

Front-end:

- **Angular 9**

Back-end:

- **NestJS**
- **MySQL**
- **TypeORM**
<hr>

## Getting Started:

Run the following command: 

``git clone https://gitlab.com/ghost_fx1337/primeholdinginternship2020.git``

## Setup environment:

1. Open task3/backend/student-form-api/src/app.module.ts

and update the below settings with your relevant MySQL configuration:

```TypeOrmModule.forRoot({ <br>
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: 'UNDEFINED',
        database: 'UNDEFINED',
        entities: [__dirname + '/**/*.entity{.ts,.js}'],
        synchronize: false,
      })
```

2. Change the ``synchronize: false`` to ``true`` in order to generate the database application entity(table).

3. Run ``npm i`` both in the front and backend folders

4. Start backend api with the ``npm run start:dev`` command

5. Start the client using ``ng s`` command

6. Go to ``http://localhost:4200/`` to view the application

Bugs:

* When editing an application, you need to manually fill the date via the date picker, I was not able to bypass this issue.

* Tha application is not responsive and works best for a laptop screen (15' 1200px 719px)