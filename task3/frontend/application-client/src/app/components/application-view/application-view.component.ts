import { Component, OnInit } from '@angular/core';
import { ApplicationService } from 'src/app/core/services/application.service';
import { ActivatedRoute } from '@angular/router';
import { Application } from 'src/app/common/interfaces/application.interface';

@Component({
  selector: 'app-application-view',
  templateUrl: './application-view.component.html',
  styleUrls: ['./application-view.component.css']
})
export class ApplicationViewComponent implements OnInit {
  public id: string;
  public applicationDetails: Application;

  constructor(
    private readonly appService: ApplicationService,
    private readonly route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe( params => {
      this.id = params.get('id');
      // console.log(this.id);
    })
    this.appService.getApplicationDetails(+this.id).subscribe((data) => {
      this.applicationDetails = data.data;
      // console.log(this.applicationDetails);
    });
  }

}
