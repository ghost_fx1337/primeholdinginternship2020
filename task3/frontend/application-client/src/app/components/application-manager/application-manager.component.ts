import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationService } from 'src/app/core/services/application.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { Application } from 'src/app/common/interfaces/application.interface';

@Component({
  selector: 'app-application-manager',
  templateUrl: './application-manager.component.html',
  styleUrls: ['./application-manager.component.css']
})
export class ApplicationManagerComponent implements OnInit {

  public applications: Application[];
  public p: number = 1;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly appService: ApplicationService,
    private readonly notificationService: NotificationService,
  ) { }

  deleteApplication(id): void {
    // console.log(id);
    this.appService.deleteApplication(id).subscribe(() => {
      this.appService.getAllApplications().subscribe((data) => {
        this.applications = data.data;
      });
      this.notificationService.success('Application deleted!');
    });
  }

  ngOnInit(): void {
    this.appService.getAllApplications().subscribe((data) => {
      this.applications = data.data;
      // console.log(this.applications);
    });
  }

}
