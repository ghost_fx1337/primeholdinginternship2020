import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const application = ( () => {
      let canvas;
      let ctx;
      let imgData;
      let pix;
      let WIDTH;
      let HEIGHT;
      let flickerInterval;
  
      const init = () => {
          canvas = document.getElementById('canvas');
          ctx = canvas.getContext('2d');
          canvas.width = WIDTH = 700;
          canvas.height = HEIGHT = 500;
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, WIDTH, HEIGHT);
          ctx.fill();
          imgData = ctx.getImageData(0, 0, WIDTH, HEIGHT);
          pix = imgData.data;
          flickerInterval = setInterval(flickering, 30);
      };
  
      const flickering = () => {
          for (let i = 0; i < pix.length; i += 4) {
              const color = (Math.random() * 255) + 50;
              pix[i] = color;
              pix[i + 1] = color;
              pix[i + 2] = color;
          }
          ctx.putImageData(imgData, 0, 0);
      };
  
      return {
          init: init
      };
  })();
  
  application.init();
  }

}
