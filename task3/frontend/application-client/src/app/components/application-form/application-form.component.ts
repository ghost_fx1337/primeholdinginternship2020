import { CommunicationPreference } from './../../common/enums/communication-preference.enum';
import { ApplicationService } from './../../core/services/application.service';
import { Component, OnInit } from '@angular/core';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormArray,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-application-form',
  templateUrl: './application-form.component.html',
  styleUrls: ['./application-form.component.css']
})
export class ApplicationFormComponent implements OnInit {

  public createApplicationForm: FormGroup;
  public radioBtn: boolean;
  public emailBtn: boolean;
  public phoneBtn: boolean;
  public model: NgbDateStruct;
  public languageProficiency: string[] = ['A1', 'A2', 'B1', 'B2', 'C1', 'C2'];
  public paramId: string;
  constructor(
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly appService: ApplicationService,
    private readonly notificationService: NotificationService,
  ) { }

  submitApplication(): void {
    const application = this.createApplicationForm.value;
    application.startDate = application.startDate.year + '-' + application.startDate.month + '-' + application.startDate.day;
    console.log(application);
    this.appService.createApplication(application).subscribe(() => {
      this.router.navigate(['/applications']);
      this.notificationService.success('Your application was successfully submitted!');
    });
  }

  editApplication(): void {
    const application = this.createApplicationForm.value;
    application.id = this.paramId;
    this.appService.updateApplication(application).subscribe(() => {
      this.router.navigate(['/applications']);
      this.notificationService.success('Your application was successfully edited!');
    });
  }

  ngOnInit(): void {
    this.createApplicationForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]],
      surname: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]],
      age: [null, Validators.required],
      phoneNumber: ['', Validators.required],
      communicationPreference: ['', Validators.required],
      englishLevel: [null, Validators.required],
      startDate: ['', Validators.required],
      skillsAndCourses: ['', Validators.required],
      presentation: ['', Validators.required],
      studyFromHome: [false, Validators.required]
    });

    this.route.paramMap.subscribe((params) => {
      this.paramId = params.get('id');
      // console.log(this.paramId);
    });

    if (this.paramId) {
      this.appService.getApplicationDetails(+this.paramId).subscribe((data) => {
        // console.log(data.data.startDate);
        (this.createApplicationForm.controls.firstName as FormControl).setValue(
          data.data.firstName
        );
        (this.createApplicationForm.controls.surname as FormControl).setValue(
          data.data.surname
        );
        (this.createApplicationForm.controls.age as FormControl).setValue(
          data.data.age
        );
        (this.createApplicationForm.controls.phoneNumber as FormControl).setValue(
          data.data.phoneNumber
        );
        (this.createApplicationForm.controls.communicationPreference as FormControl).setValue(
          data.data.communicationPreference
        );
        (this.createApplicationForm.controls.englishLevel as FormControl).setValue(
          data.data.englishLevel
        );
        (this.createApplicationForm.controls.startDate as FormControl).setValue(
          data.data.startDate
        );
        (this.createApplicationForm.controls.skillsAndCourses as FormControl).setValue(
          data.data.skillsAndCourses
        );
        (this.createApplicationForm.controls.presentation as FormControl).setValue(
          data.data.presentation
        );
        (this.createApplicationForm.controls.studyFromHome as FormControl).setValue(
          data.data.studyFromHome
        );
      });
      // console.log((this.createApplicationForm.controls.startDate as FormControl));
    }


  }

}
