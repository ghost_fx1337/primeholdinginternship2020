import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APIResponse } from 'src/app/common/api-response';
import { Application } from 'src/app/common/interfaces/application.interface';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  public constructor(private readonly httpClient: HttpClient) {}

  public getAllApplications(): Observable<APIResponse<Application[]>> {
    return this.httpClient.get<APIResponse<Application[]>>(
      `http://localhost:3000/applications`
    );
  }

  public getApplicationDetails(appId: number): Observable<APIResponse<Application>> {
    return this.httpClient.get<APIResponse<Application>>(
      `http://localhost:3000/applications/${appId}`
    );
  }

  public createApplication(application: Application): Observable<APIResponse<Application>> {
    return this.httpClient.post<APIResponse<Application>>(
      `http://localhost:3000/applications`,
      application
    );
  }

  public updateApplication(application: Application): Observable<APIResponse<Application>> {
    return this.httpClient.patch<APIResponse<Application>>(
      `http://localhost:3000/applications/${application.id}`,
      application
    );
  }

  public deleteApplication(id: string): Observable<APIResponse<Application>> {
    return this.httpClient.delete<APIResponse<Application>>(
      `http://localhost:3000/applications/${id}`
    );
  }


}
