import { ApplicationService } from './services/application.service';
import { NotificationService } from './services/notification.service';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  providers: [
    NotificationService,
    ApplicationService,
  ],
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class CoreModule {
  public constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core Module already initiated!');
    }
  }
}
