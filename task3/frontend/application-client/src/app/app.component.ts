import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'application-client';
  
  validRoute: string;

  public constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  

  public ngOnInit() {
    this.validRoute = window.location.href;
    console.log(this.validRoute)
  }
}
