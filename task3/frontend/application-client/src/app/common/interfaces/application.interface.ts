import { EnglishLevel } from '../enums/english-level.enum';
import { CommunicationPreference } from '../enums/communication-preference.enum';

export interface Application {

id: string;

firstName: string;

surname: string;

age: number;

phoneNumber: string;

communicationPreference: CommunicationPreference;

englishLevel: EnglishLevel;

startDate: string;

skillsAndCourses: string;

presentation: string;

studyFromHome: boolean;
}