import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ApplicationManagerComponent } from './components/application-manager/application-manager.component';
import { ApplicationFormComponent } from './components/application-form/application-form.component';
import { ApplicationHomeComponent } from './components/application-home/application-home.component';
import { ApplicationViewComponent } from './components/application-view/application-view.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ApplicationHomeComponent },
  { path: 'applications', component: ApplicationManagerComponent },
  { path: 'new', component: ApplicationFormComponent },
  { path: 'edit/:id', component: ApplicationFormComponent },
  { path: 'application-details/:id', component: ApplicationViewComponent },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '404' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      onSameUrlNavigation: 'reload',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}