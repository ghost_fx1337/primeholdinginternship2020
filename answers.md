**Prime Holding Internship Questions:**

•	Is it better to follow your dream or to investigate the job market when choosing a profession?

***	
There is a saying: "Find a job you love and you will never have to work a day in your life".
<br>
I personally followed the latter approach when faced with this dilemma. When I was younger I was passionate about music and spent a large portion of my time practicing my guitar skills, playing in bands and I truly loved every second of that experience. I was never steered into specific fields of study by my parents as they were always very supportive about my choices and had full confidence in me and trusted me with the decisions I made for myself. 

Even though I loved playing music I was fully aware that I would not be able to successfully persue such endeavours in Bulgaria. When you accept a certain kind of reality, the next step is always quite scary as it requires you to leave your comfort zone and explore other vast and unfamiliar areas which can be very overwhelming. 

It took my quite some time to find my passion for programming as I was not really quite sure what I wanted to do after I let go of my dream. I decided to study what is popular and prestigious. Studying law abroad was intersting and it did broaden my views significantly, however, as fascinating as this field is, it did not ignite my curiousity as much as I would have hoped. 

At that point I had the desire to work and begin developing my professional career, however, I still lacked a clear direction. For the next couple of years I worked in the support field where I broadened my understanding of the business model, as well as its structure and different elements.

At that point I knew much more about myself as well and I knew I needed a bigger challenge, which will keep me motivated, interested and most importantly, place me in an environment of constant learning and adaptation.

As a conclusion, I would say that it is much better to follow your dream if you are given an opportunity to do so, however, life is also about adaptation and making a choice like this is never wrong or right, since each path you take comes with consequences you need to be okay with at the end of the day. 

***

•	If you have the chance to address only 1 question to the CEO, what would it be? Explain why.

***

What is more important for you: skill or loyalty?

This question is important for me because it shows me what a CEO will value more in a person - their current contributions or their potential/desire for growth.

For example if a person is already trained for a position and they are executing it perfectly and the management is satisfied with that, but one day that person decides that they want to develop further and invest in themselves and grow and request assistance/ guidence from their employer.

- how would they be met, with support or distrust? 

One possible way is for the employer to encourage and invest in that person and let them grow in the direction they are passionate about. I beleive this will result in a more skilled and thankful, loyal employee who believes in his company and its leaders the same as they have believed and trusted them.

Alternatively, that employee can be met with lack of interest and no support which would not only demotivate them but make them lose faith in the company as they will feel under-appreciated. 

***

•	Describe a time where you made a mistake, and how you handled it and what have you learned from it.

*** 
In 2018 I volunteered night shifts for 7 months in a row at my old job as they were paid more and I needed the money to help out my parents. These shifts were longer (10 hours) and much more intense since we were always less people than in the regular day shifts but the work load was in fact higher. 

The work process we were taught was to try and handle multiple problems at the same time, performing one action on one account while waiting for another action to complete on another account, etc. 

The mistake I made was that I tried to multifunction 5 accounts at the same time, I had 15 windows opened on my terminal and was managing client requests as fast as possible, but it was very late and I was really tired not just from this shift but from a general lack of a good night's rest over the past months. 

What happened was that one client requested his account to be reset and all files deleted so they can begin working on their website from sctach. I deleted the wrong account by mistake due to my desire to cope with the work load and close more tickets. The damage was reversible but I was fined for my mistake which was fair. 

I felt really bad that I was punished for this as I was honestly doing my best to complete more work faster, but I get that the mistake was mine alone and I put myself in this overworked state. I took full responsibility and followed up with the client the next day and sincerely apologised for the error on my behalf.
They appreciated the honesty and the fact that we did fix the issue quickly after they reported it. 

***

